//
//  GameScene.swift
//  PTR
//
//  Created by Milan Banjanin on 10/04/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import SpriteKit
import GameplayKit

enum GameState {
    case start, playing, dead
}

enum CarPosition {
    case left, right, centre
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var car: SKSpriteNode!
    
    var buttonLeft: SKSpriteNode!
    var buttonRight: SKSpriteNode!
    
    var carPosition: CarPosition = .centre
    
    var carPositionLeft: CGPoint!
    var carPositionCentre: CGPoint!
    var carPositionRight: CGPoint!
    
    var viewFrame: CGRect! {
        didSet {
            carPositionLeft = CGPoint(x: viewFrame.width / 6, y: viewFrame.height * 0.25)
            carPositionCentre = CGPoint(x: viewFrame.width / 2, y: viewFrame.height * 0.25)
            carPositionRight = CGPoint(x: viewFrame.width / 6 * 5, y: viewFrame.height * 0.25)
        }
    }
    
    var score = 0 {
        didSet {
            scoreLabel.text = "Score: \(score)"
            if score > highscore! {
                highscore = score
            }
        }
    }
    
    var scoreLabel: SKLabelNode!
    
    var highscore: Int? {
        get {
            return UserDefaults.standard.integer(forKey: "highscore")
        }
        set(value) {
            UserDefaults.standard.set(value, forKey: "highscore")
        }
    }
    
    var highscoreLabel: SKLabelNode!
    
    var gameState: GameState = .start
    
    var startLabel: SKLabelNode!
    var gameOverLabel: SKLabelNode!
    
    var backgroundMusic: SKAudioNode!
    
    var linesTimer: Timer?
    var rocksTimer: Timer?
    var starsTimer: Timer?
    
    override func didMove(to view: SKView) {

        viewFrame = frame
        backgroundColor = UIColor.gray
        
        createCar()
        repeatLines()
        createScore()
        createLabels()
        startMusic()
        
        physicsWorld.contactDelegate = self
        physicsWorld.gravity = CGVector(dx: 0.0, dy: 0.0)
    }
   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let touch = touches.first else { return }
        
        let location = touch.location(in: self)
        let nodesAtPoint = nodes(at: location)
        
        for node in nodesAtPoint {
            if node.name == "buttonLeft" {
                moveLeft()
            } else if node.name == "buttonRight" {
                moveRight()
            }
        }
        
        switch gameState {
        case .start:
            
            let fadeOut = SKAction.fadeOut(withDuration: 0.5)
            let remove = SKAction.removeFromParent()
            let wait = SKAction.wait(forDuration: 0.5)
            
            let activateGame = SKAction.run { [unowned self] in
                self.runRocks()
                self.createButtons()
                self.runStars()
            }
            
            let sequence = SKAction.sequence([fadeOut, wait, activateGame, remove])
            startLabel.run(sequence)
            gameState = .playing
        case .playing:
            break
        case .dead:
            let scene = GameScene(fileNamed: "GameScene")!
            let transition = SKTransition.moveIn(with: SKTransitionDirection.up, duration: 1)
            self.view?.presentScene(scene, transition: transition)
            
        }
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        if contact.bodyA.node?.name == "car" && contact.bodyB.node?.name == "rock" || contact.bodyA.node?.name == "rock" && contact.bodyB.node?.name == "car" {
            if let explosion = SKEmitterNode(fileNamed: "explosion") {
                explosion.position = car.position
                addChild(explosion)
            }
            
            let sound = SKAction.playSoundFileNamed("explosion.wav", waitForCompletion: false)
            run(sound)
            
            car.removeFromParent()
            speed = 0
            gameState = .dead
            gameOverLabel.alpha = 1
            backgroundMusic.run(SKAction.stop())
            linesTimer?.invalidate()
            rocksTimer?.invalidate()
            starsTimer?.invalidate()
            createHighscore()
            
        }
        
        if contact.bodyA.node?.name == "star" && contact.bodyB.node?.name == "car" {
            contact.bodyA.node?.removeFromParent()
            let sound = SKAction.playSoundFileNamed("coin", waitForCompletion: false)
            run(sound)
            score += 10
        }
        if contact.bodyA.node?.name == "car" && contact.bodyB.node?.name == "star" {
            contact.bodyB.node?.removeFromParent()
            let sound = SKAction.playSoundFileNamed("coin", waitForCompletion: false)
            run(sound)
            score += 10
        }
        
    }
    
    func createCar() {
    
        car = SKSpriteNode()
        car.position = carPositionCentre
        
        let carTexture = SKTexture(imageNamed: "car")
        let carX = SKSpriteNode(texture: carTexture)
        carX.name = "car"
        car.addChild(carX)

        let emitter = SKEmitterNode(fileNamed: "fuse")!
        emitter.position = CGPoint(x: 0, y: -24)
        car.addChild(emitter)

        addChild(car)
        
        carX.physicsBody = SKPhysicsBody(texture: carTexture, size: carTexture.size())
        carX.physicsBody?.isDynamic = true
        carX.physicsBody?.collisionBitMask = 1
        carX.physicsBody?.categoryBitMask = 1
        carX.physicsBody?.contactTestBitMask = 1

    }
    
    func runLines() {
        
        let size = CGSize(width: frame.width / 40, height: frame.height / 4)
        
        let rectLeft = SKSpriteNode(color: .white, size: size)
        rectLeft.position = CGPoint(x: frame.width / 3, y: frame.height * 1.1)
        
        let rectRight = SKSpriteNode(color: .white, size: size)
        rectRight.position = CGPoint(x: frame.width / 3 * 2, y: frame.height * 1.1)
        
        addChild(rectLeft)
        addChild(rectRight)
        
        let moveDown = SKAction.moveBy(x: 0, y: -(frame.height * 1.25), duration: 1)
        let destroy = SKAction.removeFromParent()
        let seq = SKAction.sequence([moveDown, destroy])
        rectRight.run(seq)
        rectLeft.run(seq)
        
    }
    
    func repeatLines() {
        linesTimer = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: true, block: {
            [weak self] _ in
            self?.runLines()
        })
    }
    
    func createButtons() {
        
        let size = CGSize(width: frame.width / 2, height: frame.height)
        
        buttonLeft = SKSpriteNode(color: .clear, size: size)
        buttonLeft.position = CGPoint(x: frame.width / 4, y: frame.height / 2)
        buttonLeft.name = "buttonLeft"
        buttonLeft.zPosition = 20
        addChild(buttonLeft)
        
        buttonRight = SKSpriteNode(color: .clear, size: size)
        buttonRight.position = CGPoint(x: frame.width / 4 * 3, y: frame.height / 2)
        buttonRight.name = "buttonRight"
        buttonRight.zPosition = 20
        addChild(buttonRight)
        
    }
    
    func moveLeft() {
        switch carPosition {
        case .centre:
            let move = SKAction.moveTo(x: carPositionLeft.x, duration: 0.15)
            car.run(move)
            carPosition = .left
        case .right:
            let move = SKAction.moveTo(x: carPositionCentre.x, duration: 0.15)
            car.run(move)
            carPosition = .centre
        case .left:
            break
        }
    }
    
    func moveRight() {
        switch carPosition {
        case .centre:
            let move = SKAction.moveTo(x: carPositionRight.x, duration: 0.15)
            car.run(move)
            carPosition = .right
        case .right:
            break
        case .left:
            let move = SKAction.moveTo(x: carPositionCentre.x, duration: 0.15)
            car.run(move)
            carPosition = .centre
        }
    }
    
    func createRocks() {
        let rockTexture = SKTexture(imageNamed: "rock")
        let rock = SKSpriteNode(texture: rockTexture)
        rock.name = "rock"
        addChild(rock)
        rock.physicsBody = SKPhysicsBody(texture: rockTexture, size: rockTexture.size())
        rock.physicsBody?.isDynamic = false
        rock.physicsBody?.collisionBitMask = 1
        rock.physicsBody?.categoryBitMask = 1
        rock.physicsBody?.contactTestBitMask = 1
        
        let moveDown = SKAction.moveBy(x: 0, y: -(frame.height * 1.25), duration: 1)
        let destroy = SKAction.removeFromParent()
        let addScore = SKAction.run {
            self.score += 1
        }
        let seq = SKAction.sequence([moveDown, destroy, addScore])
        
        
        switch Int.random(in: 0...4) {
        case 0:
            rock.position = CGPoint(x: carPositionLeft.x, y: frame.height * 1.1)
        case 1:
            rock.position = CGPoint(x: carPositionCentre.x, y: frame.height * 1.1)
        case 2:
            rock.position = CGPoint(x: carPositionRight.x, y: frame.height * 1.1)
        case 3, 4:
            let rock2 = SKSpriteNode(texture: rockTexture)
            rock2.name = "rock"
            addChild(rock2)
            rock2.physicsBody = SKPhysicsBody(texture: rockTexture, size: rockTexture.size())
            rock2.physicsBody?.isDynamic = false
            rock2.physicsBody?.collisionBitMask = 1
            rock2.physicsBody?.categoryBitMask = 1
            rock2.physicsBody?.contactTestBitMask = 1
            
            switch Int.random(in: 0...2) {
            case 0:
                rock.position = CGPoint(x: carPositionRight.x, y: frame.height * 1.1)
                rock2.position = CGPoint(x: carPositionCentre.x, y: frame.height * 1.1)
                rock2.run(seq)
                
            case 1:
                rock.position = CGPoint(x: carPositionRight.x, y: frame.height * 1.1)
                rock2.position = CGPoint(x: carPositionLeft.x, y: frame.height * 1.1)
                rock2.run(seq)
            case 2:
                rock.position = CGPoint(x: carPositionLeft.x, y: frame.height * 1.1)
                rock2.position = CGPoint(x: carPositionCentre.x, y: frame.height * 1.1)
                rock2.run(seq)

            default:
                break
            }

        default:
            break
        }
        
        rock.run(seq)
    }
    
    func runRocks() {
        rocksTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: {
            [weak self] _ in
            self?.createRocks()
        })
    }
    
    func createScore() {
        scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        scoreLabel.fontSize = 24
        scoreLabel.position = CGPoint(x: frame.width - 8, y: frame.height - 8)
        scoreLabel.text = "Score: 0"
        scoreLabel.fontColor = UIColor.white
        scoreLabel.horizontalAlignmentMode = .right
        scoreLabel.verticalAlignmentMode = .top
    
        addChild(scoreLabel)
    }
    
    func createLabels() {
        startLabel = SKLabelNode(fontNamed: "Chalkduster")
        startLabel.text = "Cick to start!"
        startLabel.fontSize = 36
        startLabel.position = CGPoint(x: frame.midX, y: frame.midY)
        addChild(startLabel)
        
        gameOverLabel = SKLabelNode(fontNamed: "Chalkduster")
        gameOverLabel.text = "Game over!"
        gameOverLabel.fontSize = 36
        gameOverLabel.position = CGPoint(x: frame.midX, y: frame.midY)
        gameOverLabel.alpha = 0
        addChild(gameOverLabel)

    }
    
    func startMusic() {
        if let musicURL = Bundle.main.url(forResource: "music", withExtension: "m4a") {
            backgroundMusic = SKAudioNode(url: musicURL)
            addChild(backgroundMusic)
        }
    }
    
    func createStar() {
        let starTexture = SKTexture(imageNamed: "star")
        let star = SKSpriteNode(texture: starTexture)
        star.name = "star"
        addChild(star)
        star.physicsBody = SKPhysicsBody(texture: starTexture, size: starTexture.size())
        star.physicsBody?.isDynamic = false
        star.physicsBody?.collisionBitMask = 0
        star.physicsBody?.categoryBitMask = 0
        star.physicsBody?.contactTestBitMask = 1
        
        let moveDown = SKAction.moveBy(x: 0, y: -(frame.height * 1.25), duration: 1)
        let destroy = SKAction.removeFromParent()
        let seq = SKAction.sequence([moveDown, destroy])
        
        
        switch Int.random(in: 0...2) {
        case 0:
            star.position = CGPoint(x: carPositionLeft.x, y: frame.height * 1.1)
        case 1:
            star.position = CGPoint(x: carPositionCentre.x, y: frame.height * 1.1)
        case 2:
            star.position = CGPoint(x: carPositionRight.x, y: frame.height * 1.1)
        default:
            break
        }
        
        star.run(seq)
        
    }
    
    func runStars() {
        starsTimer = Timer.scheduledTimer(withTimeInterval: 4.21, repeats: true, block: {
            [weak self] _ in
            self?.createStar()
        })
    }

    func createHighscore() {
        highscoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        highscoreLabel.fontSize = 36
        highscoreLabel.position = gameOverLabel.position
        highscoreLabel.text = "Highscore: \(highscore!)"
        highscoreLabel.fontColor = UIColor.white
        highscoreLabel.horizontalAlignmentMode = .center
        highscoreLabel.verticalAlignmentMode = .top
        addChild(highscoreLabel)
    }
    
    
    
}
